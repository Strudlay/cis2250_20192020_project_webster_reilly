package info.hccis.courtbooking.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import info.hccis.courtbooking.entity.Member;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This DAO is used for querying data from the member table
 */
@Dao
public interface MemberDAO {
    @Insert
    public long add(Member member);
    @Update
    public void update(Member member);
    @Delete
    public void delete(Member member);
    @Query("select * from member")
    public List<Member> get();
    @Query("select * from member where id =:id")
    public Member get(int id);
    @Query("delete from member")
    public void deleteAll();
}
