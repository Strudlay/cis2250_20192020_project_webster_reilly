package info.hccis.courtbooking.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets the data to be passed to the home fragment
 */
public class HomeViewModel extends ViewModel {
    private MutableLiveData<String> mText;
    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Court Sports");
    }
    public LiveData<String> getText() {
        return mText;
    }
}