package info.hccis.courtbooking.ui.help;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets the data to be passed to the help fragment
 */
public class HelpViewModel extends ViewModel {
    private MutableLiveData<String> mText;
    public HelpViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This application has a menu on the top left. You can access the Members or Firestore data" +
                "\n\nThe Members option, will allow you select a member and do the following: " +
                "\n1. Add member to contacts" +
                "\n2. Backup member data to cloud storage" +
                "\n3. Email member data" +
                "\n\n The Firestore option, will allow you to see which members have been backed up to cloud storage.");
    }
    public LiveData<String> getText() {
        return mText;
    }
}
