package info.hccis.courtbooking.entity;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import info.hccis.courtbooking.MainActivity;
import info.hccis.courtbooking.api.JsonApi;
import info.hccis.courtbooking.ui.user.UsersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets dummy data and loads all the user data from the API
 */
public class UserContent {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<User> USERS = new ArrayList<User>();
    public static List<User> getUsersFromRoom(){
        User newUser = new User();
        newUser.setUserId(6);
        newUser.setUserTypeDescription("Admin");
        newUser.setUsername("steve@test.com");
        newUser.setFirstName("Steve");
        newUser.setLastName("French");
        newUser.setUserTypeCode(1);
        newUser.setCreatedDateTime("2020-01-02 14:40:08");
        //Test the room add functionality
        try {
            MainActivity.myAppDatabase.userDAO().add(newUser);
        }catch(Exception e){
            MainActivity.myAppDatabase.userDAO().update(newUser);
        }
        Log.d("bjm", "Added user");
        List<User> usersBack = MainActivity.myAppDatabase.userDAO().get();
        Log.d("bjm", "loaded from room db, size="+ usersBack.size());
        Log.d("bjm", "Here they are");
        for(User current: usersBack){
            Log.d("bjm", current.toString());
        }
        Log.d("bjm", "that's it");
        return null;
    }
    /**
     * Load the users.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the USERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */

    public static void loadUsers() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(User.USER_ACCESS_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonApi jsonApi = retrofit.create(JsonApi.class);
        Call<List<User>> call = jsonApi.getUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code" + response.code());
                    return;
                }
                List<User> users = response.body();
                Log.d("bjm", "data back from service call #returned=" + users.size());
                UserContent.USERS.clear();
                UserContent.USERS.addAll(users);
                UsersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());
            }
        });
    }
}