package info.hccis.courtbooking.member;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import info.hccis.courtbooking.entity.Member;
import info.hccis.courtbooking.member.MembersFragment.OnListFragmentInteractionListener;

import java.util.List;

import info.hccis.courtbooking.R;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets up a recycler view adapter to be used to display member data
 */
public class MyMemberListRecyclerViewAdapter extends RecyclerView.Adapter<MyMemberListRecyclerViewAdapter.ViewHolder> {
    private final List<Member> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyMemberListRecyclerViewAdapter(List<Member> items, OnListFragmentInteractionListener listener) {
        this.mValues = items;
        this.mListener = listener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_members, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(Integer.toString(mValues.get(position).getId()));
        holder.mAddress.setText(mValues.get(position).getAddress());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mAddress;
        public Member mItem;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.member_id);
            mAddress = (TextView) view.findViewById(R.id.member_address);
        }
        @Override
        public String toString() {
            return super.toString() + " '" + mAddress.toString() + "'";
        }
    }
}