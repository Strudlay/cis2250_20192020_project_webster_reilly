package info.hccis.courtbooking.ui.firestore;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.concurrent.Executor;

import info.hccis.courtbooking.R;
import info.hccis.courtbooking.entity.User;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class accesses the Firestore database to show which members have been backed up
 */
public class FirestoreFragment extends Fragment {
    private TextView firestoreText;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String data;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_firestore, container, false);
        firestoreText = root.findViewById(R.id.firestore_text);
        db.collection("members").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                String data = "";
                for(QueryDocumentSnapshot snapshots : queryDocumentSnapshots){
                    User user = snapshots.toObject(User.class);
                    data += "User Id: " + user.getUserId() + " " + user.getFirstName() + " " +user.getLastName() + "\n\n";
                }
                firestoreText.setText(data);
            }
        });
        return root;
    }
}
