package info.hccis.courtbooking.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets up the table name and details for the user entity
 */
@Entity(tableName = "user")
@SuppressWarnings("serial")
public class User implements Serializable {
    public static final String USER_ACCESS_API = "http://hccis.info:8080/court/rest/UserAccessService/";
    @ColumnInfo(name="userTypeDescription")
    private String userTypeDescription;
    @ColumnInfo(name="userId")
    @PrimaryKey(autoGenerate = true)
    private Integer userId;
    @ColumnInfo(name="username")
    private String username;
    @ColumnInfo(name="lastName")
    private String lastName;
    @ColumnInfo(name="firstName")
    private String firstName;
    @ColumnInfo(name="userTypeCode")
    private Integer userTypeCode;
    @ColumnInfo(name="createdDateTime")
    private String createdDateTime;

    public String getUserTypeDescription() {
        return userTypeDescription;
    }

    public void setUserTypeDescription(String userTypeDescription) {
        this.userTypeDescription = userTypeDescription;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Integer getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(Integer userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }
    @Override
    public String toString() {
        return "\nuserId: " + this.userId + "\nusername: " + this.username + "\nlast name: " + this.lastName + "\nfirst name: "
                + this.firstName + "\nuserTypeCode: " + this.userTypeCode + "\nuserTypeDescription: " + this.userTypeDescription
                + "\ncreatedDateTime: " + this.createdDateTime;
    }
}
