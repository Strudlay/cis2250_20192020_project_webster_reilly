package info.hccis.courtbooking;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;
import android.view.Menu;
import android.widget.Toast;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import info.hccis.courtbooking.entity.Member;
import info.hccis.courtbooking.entity.MemberContent;
import info.hccis.courtbooking.member.MembersFragment;
import info.hccis.courtbooking.entity.User;
import info.hccis.courtbooking.entity.UserContent;
import info.hccis.courtbooking.database.MyAppDatabase;
import info.hccis.courtbooking.ui.user.UserFragment;
import info.hccis.courtbooking.ui.user.UsersFragment;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class is the main activity for the application. From here navigation is setup and run
 */
public class MainActivity extends AppCompatActivity implements
        MembersFragment.OnListFragmentInteractionListener,
        UsersFragment.OnListFragmentInteractionListener, UserFragment.OnFragmentInteractionListener, Serializable {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;
    private static  NavController navController = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_members_list, R.id.nav_member,
                R.id.nav_user_list, R.id.nav_user, R.id.nav_about, R.id.nav_help)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        myAppDatabase = Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"courtdb").allowMainThreadQueries().build();
        //Test the Room functionality
        MemberContent.getMembersFromRoom();
        UserContent.getUsersFromRoom();
    }
    public static NavController getNavController(){
        return navController;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    @Override
    public void onListFragmentInteraction(Member item){
        Log.d("bjm", "item communicated from fragment: " + item.toString());
        getNavController().navigate(R.id.nav_member);
    }
    @Override
    public void onListFragmentInteraction(User item) {
        Log.d("bjm", "item communicated from fragment: " + item.toString());
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putString("user", gson.toJson(item));
        getNavController().navigate(R.id.nav_user, bundle);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_about:
                getNavController().navigate(R.id.nav_about);
                return true;
            case R.id.action_help:
                getNavController().navigate(R.id.nav_help);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onFragmentInteractionAddContact(User item) {
        /* BJM 20200131 Code to add a contact */
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, item.getFirstName() + " " + item.getLastName());
        startActivity(intent);
    }
    @Override
    public void onFragmentInteractionSendEmail(User item) {
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_SUBJECT, "Courtbooking notification");
        email.putExtra(Intent.EXTRA_TEXT, item.toString());
        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email client :"));
    }
    @Override
    public void onFragmentInteractionBackupUser(User item) {
        // Create a new user with a first and last name
        Map<String, Object> member = new HashMap<>();
        member.put("userId", item.getUserId());
        member.put("username", item.getUsername());
        member.put("firstName", item.getFirstName());
        member.put("lastName", item.getLastName());
        member.put("userTypeCode", item.getUserTypeCode());
        member.put("userTypeDescription", item.getUserTypeDescription());
        member.put("createdDateTime", item.getCreatedDateTime());
        // Add a new document with a generated ID
        Toast.makeText(this, "Member " + item.getFirstName() + item.getLastName() + " backed up to Firestore.", Toast.LENGTH_SHORT).show();
        db.collection("members").document(item.getUserId().toString()).set(member);
    }
}