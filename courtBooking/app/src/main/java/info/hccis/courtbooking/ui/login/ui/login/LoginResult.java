package info.hccis.courtbooking.ui.login.ui.login;

import androidx.annotation.Nullable;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class get the authentication result : success (user details) or error message.
 */
class LoginResult {
    @Nullable
    private LoggedInUserView success;
    @Nullable
    private Integer error;
    LoginResult(@Nullable Integer error) {
        this.error = error;
    }
    LoginResult(@Nullable LoggedInUserView success) {
        this.success = success;
    }
    @Nullable
    LoggedInUserView getSuccess() {
        return success;
    }
    @Nullable
    Integer getError() {
        return error;
    }
}