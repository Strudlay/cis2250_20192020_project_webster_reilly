package info.hccis.courtbooking.entity;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.hccis.courtbooking.MainActivity;
import info.hccis.courtbooking.api.JsonApi;
import info.hccis.courtbooking.member.MembersFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets dummy data and loads all the member data from the API
 */
public class MemberContent {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<Member> MEMBERS = new ArrayList<Member>();
    public static List<Member> getMembersFromRoom(){
        Member newMember = new Member();
        newMember.setId(6);
        newMember.setUserId(6);
        newMember.setPhoneCell("902555555");
        newMember.setPhoneHome("902555555");
        newMember.setPhoneWork("902555555");
        newMember.setAddress("224 Euston Street");
        newMember.setStatus(1);
        //Test the room add functionality
        try {
            MainActivity.myAppDatabase.memberDAO().add(newMember);
        }catch(Exception e){
            MainActivity.myAppDatabase.memberDAO().update(newMember);
        }
        Log.d("bjm", "Added member");

        List<Member> membersBack = MainActivity.myAppDatabase.memberDAO().get();
        Log.d("bjm", "loaded from room db, size="+ membersBack.size());

        Log.d("bjm", "Here they are");
        for(Member current: membersBack){
            Log.d("bjm", current.toString());
        }
        Log.d("bjm", "that's it");
        return null;
    }
    /**
     * Load the members.  This method will use the rest service to provide the data.  The reason it is
     * in this class is because it is changing the value to the MEMBERS list.  This is the list which
     * is used to back the RecyclerView.
     *
     * @author BJM taken from Alex/Thomas' presentation.
     * @since 20200116
     */
    public static void loadMembers() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Member.MEMBER_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonApi jsonApi = retrofit.create(JsonApi.class);
        Call<List<Member>> call = jsonApi.getMembers();
        Log.d("bjm", "Hit 1");
        call.enqueue(new Callback<List<Member>>() {
            @Override
            public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                Log.d("bjm", "Hit 2");
                if (!response.isSuccessful()) {
                    Log.d("bjm", "Code" + response.code());
                    return;
                }
                List<Member> members = response.body();
                Log.d("bjm", "data back from service call #returned=" + members.size());
                Log.d("bjm", "Hit 3");
                MemberContent.MEMBERS.clear();
                MemberContent.MEMBERS.addAll(members);
                MembersFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<List<Member>> call, Throwable t) {
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());
            }
        });
    }
}