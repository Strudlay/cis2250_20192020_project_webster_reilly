package info.hccis.courtbooking.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import info.hccis.courtbooking.entity.Member;
import info.hccis.courtbooking.dao.MemberDAO;
import info.hccis.courtbooking.entity.User;
import info.hccis.courtbooking.dao.UserDAO;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets up the entities for the RoomDatabase
 */
@Database(entities = {Member.class, User.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {
    public abstract MemberDAO memberDAO();
    public abstract UserDAO userDAO();
}
