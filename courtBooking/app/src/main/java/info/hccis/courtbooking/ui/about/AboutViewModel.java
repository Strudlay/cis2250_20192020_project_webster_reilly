package info.hccis.courtbooking.ui.about;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets the data to be passed to the about fragment
 */
public class AboutViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public AboutViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("\nCIS 2250 Mobile Application Development \nSports Courts \nReilly Webster \nFebruary 4th 2020 " +
                "\n\n This program allows the user to see the member data within the court database using an API." +
                "\n\n The top left corner of the main screen gives you access to an activity drawer, which allows you to navigate to various screens." +
                "\n\n The welcome screen contains a carousel of images." +
                "\n\n When viewing a member the user has the option to add them as a contact, backup the user to a Firestore database, or send the data through an email." +
                "\n\n The user may also view the members stored to the Firestore under the Firestore fragment.");
    }
    public LiveData<String> getText() {
        return mText;
    }
}
