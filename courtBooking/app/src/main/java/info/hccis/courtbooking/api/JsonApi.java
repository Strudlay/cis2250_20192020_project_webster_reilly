package info.hccis.courtbooking.api;

import java.util.List;

import info.hccis.courtbooking.entity.Member;
import info.hccis.courtbooking.entity.User;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This interface is used for accessing the list of data from the api for the content classes
 */
public interface JsonApi {
    @GET("user")
    Call<List<User>> getUsers();
    @GET("member")
    Call<List<Member>> getMembers();
}
