package info.hccis.courtbooking.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import info.hccis.courtbooking.entity.User;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This DAO is used for querying data from the user table
 */
@Dao
public interface UserDAO {
    @Insert
    public long add(User user);
    @Update
    public void update(User user);
    @Delete
    public void delete(User user);
    @Query("select * from user")
    public List<User> get();
    @Query("select * from user where userId =:userId")
    public User get(int userId);
    @Query("delete from user")
    public void deleteAll();
}
