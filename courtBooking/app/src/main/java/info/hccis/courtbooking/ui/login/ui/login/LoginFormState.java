package info.hccis.courtbooking.ui.login.ui.login;

import androidx.annotation.Nullable;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class checks data validation state of the login form.
 */
class LoginFormState {
    @Nullable
    private Integer usernameError;
    @Nullable
    private Integer passwordError;
    private boolean isDataValid;
    LoginFormState(@Nullable Integer usernameError, @Nullable Integer passwordError) {
        this.usernameError = usernameError;
        this.passwordError = passwordError;
        this.isDataValid = false;
    }
    LoginFormState(boolean isDataValid) {
        this.usernameError = null;
        this.passwordError = null;
        this.isDataValid = isDataValid;
    }
    @Nullable
    Integer getUsernameError() {
        return usernameError;
    }
    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }
    boolean isDataValid() {
        return isDataValid;
    }
}