package info.hccis.courtbooking.entity;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets up the table name and details for the member entity
 */
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity(tableName = "member")
public class Member {
    public static final String MEMBER_API = "http://hccis.info:8080/court/rest/MemberService/";
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    @ColumnInfo(name = "userId")
    private Integer userId;
    @ColumnInfo(name = "phoneCell")
    private String phoneCell;
    @ColumnInfo(name = "phoneHome")
    private String phoneHome;
    @ColumnInfo(name = "phoneWork")
    private String phoneWork;
    @ColumnInfo(name = "address")
    private String address;
    @ColumnInfo(name = "status")
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPhoneCell() {
        return phoneCell;
    }

    public void setPhoneCell(String phoneCell) {
        this.phoneCell = phoneCell;
    }

    public String getPhoneHome() {
        return phoneHome;
    }

    public void setPhoneHome(String phoneHome) {
        this.phoneHome = phoneHome;
    }

    public String getPhoneWork() {
        return phoneWork;
    }

    public void setPhoneWork(String phoneWork) {
        this.phoneWork = phoneWork;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    @Override
    public String toString() {
        return "\n#" + id + "\nuserId: " + this.userId + "\ncellphone: " + this.phoneCell + "\nhome phone: " + this.phoneHome + "\nwork phone: " + this.phoneWork + "\naddress: " + this.address + "\nstatus: " + this.status;
    }
}