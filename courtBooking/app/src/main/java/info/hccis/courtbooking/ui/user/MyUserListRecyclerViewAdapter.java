package info.hccis.courtbooking.ui.user;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import info.hccis.courtbooking.R;
import info.hccis.courtbooking.entity.User;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class sets up a recycler view adapter to be used to display user data
 */
public class MyUserListRecyclerViewAdapter extends RecyclerView.Adapter<MyUserListRecyclerViewAdapter.ViewHolder> {
    private final List<User> mValues;
    private final UsersFragment.OnListFragmentInteractionListener mListener;
    public MyUserListRecyclerViewAdapter(List<User> items, UsersFragment.OnListFragmentInteractionListener listener) {
        this.mValues = items;
        this.mListener = listener;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_users, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mUserId.setText(Integer.toString(mValues.get(position).getUserId()));
        holder.mFirstName.setText(mValues.get(position).getFirstName() + " " + mValues.get(position).getLastName());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mUserId;
        public final TextView mFirstName;
        public User mItem;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mUserId = (TextView) view.findViewById(R.id.user_userId);
            mFirstName = (TextView) view.findViewById(R.id.user_firstName);
        }
        @Override
        public String toString() {
            return super.toString() + " '" + mFirstName.toString() + "'";
        }
    }
}