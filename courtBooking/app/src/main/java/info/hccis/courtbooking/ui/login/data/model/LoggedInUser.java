package info.hccis.courtbooking.ui.login.data.model;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {
    private String userId;
    private String displayName;
    public LoggedInUser(String userId, String displayName) {
        this.userId = userId;
        this.displayName = displayName;
    }
    public String getUserId() {
        return userId;
    }
    public String getDisplayName() {
        return displayName;
    }
}
