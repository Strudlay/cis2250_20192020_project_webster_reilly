package info.hccis.courtbooking.ui.user;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.google.gson.Gson;
import java.io.Serializable;
import info.hccis.courtbooking.R;
import info.hccis.courtbooking.entity.User;
/**
 * Name: Reilly Webster
 * Subject: CIS 2250 Mobile Application Development
 * Date: February 7th, 2020
 * Purpose: This class displays the member fragment layout
 */
public class UserFragment extends Fragment implements Serializable {
    private User user;
    private ImageButton buttonAddContact, buttonSendEmail, buttonFirestoreBackup;
    private OnFragmentInteractionListener mListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String camperJson = getArguments().getString("user");
            Gson gson = new Gson();
            user = gson.fromJson(camperJson, User.class);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        buttonAddContact = view.findViewById(R.id.imageButtonAdd);
        buttonSendEmail = view.findViewById(R.id.imageButtonSend);
        buttonFirestoreBackup = view.findViewById(R.id.imageButtonBackup);
        buttonFirestoreBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionBackupUser(user);
            }
        });
        buttonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(user);
            }
        });
        buttonSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionSendEmail(user);
            }
        });
        TextView memberInfo = (TextView) view.findViewById(R.id.member_info);
        String memberText = "\nUser Id: " + user.getUserId() +
                "\nUsername: " + user.getUsername() +
                "\nFirst name: " + user.getFirstName() +
                "\nLast name: " + user.getLastName() +
                "\nUser type: " + user.getUserTypeCode() +
                "\nUser type description: " + user.getUserTypeDescription() +
                "\nCreated date time: " + user.getCreatedDateTime();
        memberInfo.setText(memberText);
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionAddContact(User item);
        void onFragmentInteractionSendEmail(User user);
        void onFragmentInteractionBackupUser(User user);
    }
}